require 'watir'

#declarations
Before do
  @homepage = "taskeasy.com"
  @tips_page = "taskeasy.com/taskeasyTips"
  @browser = Watir::Browser.new
end
After do
  @browser.close
end