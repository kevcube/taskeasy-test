Given(/^a web browser$/) do
  #nothing
end

When(/^navigating to taskeasy.com$/) do
  @browser.goto @homepage
  @browser.wait
end

Then(/^the TaskEasy homepage should load properly$/) do
  expect(@browser.text.include?("How it works")).to be true
end

Given(/^the browser is at taskeasy.com$/) do
  @browser.goto @homepage
  @browser.wait
end

When(/^clicking TaskEasy Tips$/) do
  @browser.link(:text =>"TaskEasy Tips").click
  @browser.wait
end

Then(/^the TaskEasy tips page should load$/) do
  expect(@browser.text.include?("What is TaskEasy Tips?")).to be true
end

Given(/^the browser is at taskeasy.com\/taskeasyTips$/) do
  @browser.goto @tips_page
  @browser.wait
end

When(/^finding and selecting common lawn diseases$/) do
  @browser.i(:id =>"summerTipsPlus").click
  @browser.link(:id =>"summer_common_lawn_diseases").click
end

Then(/^we should be on the common lawn diseases page$/) do
    expect(@browser.title.include?("mysterious case of common lawn diseases")).to be true
end

Given(/^the browser is at taskeasy.com\/taskeasyTips\/MOWLAWN\/commonLawnDiseases$/) do
  @browser.goto @tips_page + "/MOWLAWN/commonLawnDiseases"
  @browser.wait
end

When(/^clicking Want help with your lawn mowing$/) do
  @browser.link(:text =>"Want help with your lawn mowing?").click
  @browser.wait
end

Then(/^taskeasy.com\/lawnmowing should properly load$/) do
  expect(@browser.text.include?("We make Lawn Mowing easy")).to be true
end