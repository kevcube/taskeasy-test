Feature: Getting help with lawn service
	As lawnservice customer
	I want to be able to navigate to TaskEasy, find tips about my lawn, learn about common lawn diseases, and schedule some lawn maintenance
	So that I may know more about what my lawn needs, and care for it properly

Scenario: navigate to website
	Given a web browser
	When navigating to taskeasy.com
	Then the TaskEasy homepage should load properly

Scenario: click TaskEasy Tips
	Given the browser is at taskeasy.com
	When clicking TaskEasy Tips
	Then the TaskEasy tips page should load

Scenario: find info on common lawn diseases
	Given the browser is at taskeasy.com/taskeasyTips
	When finding and selecting common lawn diseases
	Then we should be on the common lawn diseases page

Scenario: selecting the button to get help with lawn mowing
	Given the browser is at taskeasy.com/taskeasyTips/MOWLAWN/commonLawnDiseases
	When clicking Want help with your lawn mowing
	Then taskeasy.com/lawnmowing should properly load
